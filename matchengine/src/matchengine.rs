use darksteel::modules::distributed::node::NodeConfig;
use darksteel::modules::distributed::{discovery::Discovery, node::Node};
use darksteel::prelude::*;
use async_raft::{NodeId, State};
use serde::{Deserialize, Serialize};
use std::net::IpAddr;
use std::ops::Range;
use std::time::Duration;
use tokio::time::interval;

use crate::orderbook::{MatchResult, Order, OrderBook, OrderCmd, Side};
use matchengine::match_engine_server::{MatchEngine, MatchEngineServer};
use matchengine::{HelloReply, HelloRequest, PlaceOrderReply, PlaceOrderRequest};

use tonic::{transport::server::Router, transport::Server, Request, Response, Status};

pub mod matchengine {
    tonic::include_proto!("matchengine");
}

struct Local(u8, Range<u32>);

impl Local {
    fn new(subnet: u8, range: Range<u32>) -> Self {
        Self(subnet, range)
    }
}

#[darksteel::async_trait]
impl Discovery for Local {
    async fn discover(&self) -> anyhow::Result<Vec<IpAddr>> {
        Ok(self
            .1
            .clone()
            .into_iter()
            .map(|index| format!("127.0.{}.{}", self.0, index).parse().unwrap())
            .collect())
    }
}

#[darksteel::identity("StateMachine")]
#[derive(Clone, Default, Serialize, Deserialize)]
struct StateMachine {
    state: OrderBook,
    result: Vec<MatchResult>,
}

impl StateMachine {
    fn state(&self) -> OrderBook {
        self.state.clone()
    }

    fn get_match_result(&self) -> Vec<MatchResult> {
        self.result.clone()
    }
}

#[darksteel::distributed]
impl StateMachine {
    fn update(&mut self, cmd: OrderCmd, side: Side, order: Order) {
        let mut mr = self.state.place_order(side, &order);
        self.result.append(&mut mr)
    }
}

pub struct MatchEngineService {
    index: i32,
    raft_node: Node,
    peer_addr: Vec<String>,
}

#[tonic::async_trait]
impl matchengine::match_engine_server::MatchEngine for MatchEngineService {
    async fn say_hello(
        &self,
        request: Request<HelloRequest>,
    ) -> Result<Response<HelloReply>, Status> {
        println!("Got a request: {:?}", request);

        let reply = matchengine::HelloReply {
            message: format!("Hello {}!", request.into_inner().name).into(),
        };

        Ok(Response::new(reply))
    }

    async fn place_order(
        &self,
        request: Request<PlaceOrderRequest>,
    ) -> Result<Response<PlaceOrderReply>, Status> {
        println!("Got a request: {:?}", request);
        //TODO: Add more implementation
        let reply = matchengine::PlaceOrderReply {
            sequance: 0,
            leader: "[::1]:50051".to_string(),
        };

        Ok(Response::new(reply))
    }
}

impl MatchEngineService {
    pub async fn new(index: i32, cluster_name: String) -> Self {
        let result = Node::build_with_config(NodeConfig::new(
            // Make sure the IPs are on different subnets across tests
            format!("127.0.2.{}", index).parse().unwrap(),
            cluster_name,
            Local::new(0, 1..5),
        ))
        .with::<StateMachine>()
        .finish()
        .await;

        let mynode = match result {
            Ok(node) => node,
            Err(e) => panic!("Error when creating node. {}", e),
        };
        
        //let addr =  format!("127.0.1.{}:50051", index).parse().unwrap();
        Self {
            index: index,
            raft_node: mynode,
            peer_addr: vec![],
        }
    }

    pub async fn initialise(&self)  {
        self.raft_node.initialise().await;
    }


    pub async fn leader(&self) -> Option<NodeId> {
        self.raft_node.leader().await
    }
}

#[tokio::test(flavor = "multi_thread", worker_threads = 4)]
async fn me_service_form() -> anyhow::Result<()> {
    let mut services = Vec::new();
    let mut interval = interval(Duration::from_secs(2));
    interval.tick().await;

    for index in 1..5 {
        let service = MatchEngineService::new(index, "matchengine_service_cluster".to_string()).await;

        services.push(service);
    }

    interval.tick().await;

    for service in &services {
        service.initialise().await;
    }

    interval.tick().await;

    for service in &services {
        assert_ne!(service.leader().await, None);
    }

    Ok(())
}

#[tokio::test(flavor = "multi_thread", worker_threads = 4)]
async fn me_cluster_form() -> anyhow::Result<()> {
    let mut nodes = Vec::new();
    let mut interval = interval(Duration::from_secs(2));
    interval.tick().await;

    for index in 1..5 {
        let node = Node::build_with_config(NodeConfig::new(
            // Make sure the IPs are on different subnets across tests
            format!("127.0.0.{}", index).parse().unwrap(),
            "test",
            Local::new(0, 1..5),
        ))
        .with::<StateMachine>()
        .finish()
        .await?;

        nodes.push(node);
    }

    for controller in &nodes {
        controller.initialise().await.unwrap();
    }

    interval.tick().await;

    for node in &nodes {
        assert_ne!(node.leader().await, None);
    }

    Ok(())
}

#[tokio::test(flavor = "multi_thread", worker_threads = 4)]
async fn me_cluster_distribute() -> anyhow::Result<()> {
    let mut nodes = Vec::new();
    let mut interval = interval(Duration::from_secs(2));
    interval.tick().await;

    for index in 1..5 {
        let node = Node::build_with_config(NodeConfig::new(
            // Make sure the IPs are on different subnets across tests
            format!("127.0.1.{}", index).parse().unwrap(),
            "test",
            Local::new(0, 1..5),
        ))
        .with::<StateMachine>()
        .finish()
        .await?;

        nodes.push(node);
    }

    for node in &nodes {
        node.initialise().await.unwrap();
    }

    interval.tick().await;

    let o1 = Order::new(500.0, 10.0, 1);
    let o2 = Order::new(500.0, 10.0, 2);

    for node in &nodes {
        if Some(node.id()) == node.leader().await {
            let mutation1 = StateMachine::create_update(OrderCmd::Place, Side::Buy, o1);
            node.commit(mutation1).await?;

            let mutation1 = StateMachine::create_update(OrderCmd::Place, Side::Sell, o2);
            node.commit(mutation1).await?;
        }
    }

    interval.tick().await;

    let mr = MatchResult::new(&o2, &o1, o1.volume);

    for node in &nodes {
        if let Some(state) = node.state::<StateMachine>().await {
            assert_eq!(state.get_match_result().len(), 1);
            assert_eq!(state.get_match_result()[0], mr);
            println!("Match Result: {:?}", state.get_match_result()[0])
        } else {
            assert!(false)
        }
    }

    Ok(())
}
