use std::cmp::Ordering;
use std::collections::BinaryHeap;
extern crate uuid;
use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Side {
    Buy,
    Sell,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum OrderCmd {
    Place,
    Cancel,
}

#[derive(Debug)]
pub enum OrderType {
    Market,
    Limit,
}

#[derive(Debug, Clone, Copy,Serialize, Deserialize)]
pub struct Order {
    pub price: f64,
    pub volume: f64,
    //pub id: uuid::Uuid,
    pub sequance: u64,
}

impl Order {
    pub fn new(price: f64, size: f64, sequance: u64) -> Self {
        Self {
            price: price,
            volume: size,
            sequance: sequance,
        }
    }
}

impl PartialEq for Order {
    fn eq(&self, other: &Self) -> bool {
        (self.sequance == other.sequance)
            && (self.price == other.price)
            && (self.volume == self.volume)
    }
}
impl Eq for Order {}

#[derive(Debug, Clone, Copy,Serialize, Deserialize)]
pub struct BidRecord {
    pub order: Order,
}

impl PartialEq for BidRecord {
    fn eq(&self, other: &Self) -> bool {
        self.order == other.order
    }
}

impl Eq for BidRecord {}

impl Ord for BidRecord {
    fn cmp(&self, other: &Self) -> Ordering {
        // Notice that the we flip the ordering on costs.
        // In case of a tie we compare positions - this step is necessary
        // to make implementations of `PartialEq` and `Ord` consistent.
        if self.order.price > other.order.price {
            Ordering::Greater
        } else if self.order.price < other.order.price {
            Ordering::Less
        } else {
            if self.order.sequance < other.order.sequance {
                Ordering::Greater
            } else if self.order.sequance > other.order.sequance {
                Ordering::Less
            } else {
                Ordering::Equal
            }
        }
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for BidRecord {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

#[derive(Debug, Clone, Copy,Serialize, Deserialize)]
pub struct AskRecord {
    pub order: Order,
}

impl PartialEq for AskRecord {
    fn eq(&self, other: &Self) -> bool {
        self.order == other.order
    }
}

impl Eq for AskRecord {}

impl Ord for AskRecord {
    fn cmp(&self, other: &Self) -> Ordering {
        // Notice that the we flip the ordering on costs.
        // In case of a tie we compare positions - this step is necessary
        // to make implementations of `PartialEq` and `Ord` consistent.
        if self.order.price > other.order.price {
            Ordering::Less
        } else if self.order.price < other.order.price {
            Ordering::Greater
        } else {
            if self.order.sequance < other.order.sequance {
                Ordering::Greater
            } else if self.order.sequance > other.order.sequance {
                Ordering::Less
            } else {
                Ordering::Equal
            }
        }
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for AskRecord {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

/// main OrderBook structure
#[derive(Clone, Default, Serialize, Deserialize, Debug)]
pub struct OrderBook {
    bid: BinaryHeap<BidRecord>,
    ask: BinaryHeap<AskRecord>,
    sequance: u64,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct MatchResult {
    pub taker: Order,
    pub maker: Order,
    pub matches: f64,
}

impl PartialEq for MatchResult {
    fn eq(&self, other: &Self) -> bool {
        (self.taker == other.taker) && (self.maker == other.maker) && (self.matches == other.matches)
    }
}


impl MatchResult {
    /// creates new matchresult
    pub fn new(taker: &Order, maker: &Order, matches: f64) -> Self {
        Self {
            taker: taker.clone(),
            maker: maker.clone(),
            matches: matches,
        }
    }
}

impl BidRecord {
    /// creates new bidrecord
    pub fn new(order: &Order) -> Self {
        Self {
            order: order.clone(),
        }
    }
}

impl AskRecord {
    /// creates new orderbook
    pub fn new(order: &Order) -> Self {
        Self {
            order: order.clone(),
        }
    }
}

impl OrderBook {
    /// creates new orderbook
    pub fn new() -> Self {
        Self {
            bid: BinaryHeap::new(),
            ask: BinaryHeap::new(),
            sequance: 0,
        }
    }

    /// get current bid
    pub fn best_bid(&self) -> Option<Order> {
        match self.bid.peek() {
            None => None,
            Some(&or) => Some(or.order),
        }
    }

    /// get current ask
    pub fn best_ask(&self) -> Option<Order> {
        match self.ask.peek() {
            None => None,
            Some(&or) => Some(or.order),
        }
    }

    pub fn place_order(&mut self, side: Side, order: &Order) -> Vec<MatchResult> {
        if order.sequance > self.sequance {
            self.sequance = order.sequance;
            match side {
                Side::Buy => {
                    return self.match_bid_order(order);
                }
                Side::Sell => {
                    return self.match_ask_order(order);
                }
            }
        }
        return vec![];
    }

    pub fn match_bid_order(&mut self, order: &Order) -> Vec<MatchResult> {
        match self.ask.peek().as_mut() {
            None => {
                self.bid.push(BidRecord::new(order));
                return vec![];
            }
            Some(ar) => {
                let best_ask = ar.order;
                if best_ask.price > order.price {
                    self.bid.push(BidRecord::new(order));
                    return vec![];
                } else {
                    // Take order
                    if best_ask.volume >= order.volume {
                        let matches = order.volume;
                        let mr = MatchResult::new(order, &best_ask, matches);

                        if best_ask.volume > order.volume {
                            let new_best_ask = Order::new(
                                best_ask.price,
                                best_ask.volume - matches,
                                best_ask.sequance,
                            );
                            self.ask.push(AskRecord::new(&new_best_ask));
                        } else {
                            self.ask.pop();
                        }
                        return vec![mr];
                    } else {
                        let mut mrs = vec![MatchResult::new(order, &best_ask, best_ask.volume)];
                        let new_order =
                            Order::new(order.price, order.volume - best_ask.volume, order.sequance);
                        self.ask.pop();
                        let mut new_mrs = self.match_bid_order(&new_order);
                        mrs.append(&mut new_mrs);
                        return mrs;
                    }
                }
            }
        }
    }

    pub fn match_ask_order(&mut self, order: &Order) -> Vec<MatchResult> {
        match self.bid.peek().as_mut() {
            None => {
                self.ask.push(AskRecord::new(order));
                return vec![];
            }
            Some(ar) => {
                let best_bid = ar.order;
                if best_bid.price < order.price {
                    self.ask.push(AskRecord::new(order));
                    return vec![];
                } else {
                    // Take order
                    if best_bid.volume >= order.volume {
                        let matches = order.volume;
                        let mr = MatchResult::new(order, &best_bid, matches);

                        if best_bid.volume > order.volume {
                            let new_best_bid = Order::new(
                                best_bid.price,
                                best_bid.volume - matches,
                                best_bid.sequance,
                            );
                            self.bid.push(BidRecord::new(&new_best_bid));
                        } else {
                            self.bid.pop();
                        }
                        return vec![mr];
                    } else {
                        let mut mrs = vec![MatchResult::new(order, &best_bid, best_bid.volume)];
                        let new_order =
                            Order::new(order.price, order.volume - best_bid.volume, order.sequance);
                        self.bid.pop();
                        let mut new_mrs = self.match_ask_order(&new_order);
                        mrs.append(&mut new_mrs);
                        return mrs;
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn basic_best_bid() {
        let mut ob = OrderBook::new();
        let o1 = Order::new(100.22, 20.0, 1);
        let o2 = Order::new(100.22, 20.0, 2);
        let o3 = Order::new(100.24, 20.0, 3);
        let o4 = Order::new(100.20, 20.0, 4);

        ob.place_order(Side::Buy, &o1);
        assert_eq!(ob.best_bid(), Some(o1));
        ob.place_order(Side::Buy, &o2);
        assert_eq!(ob.best_bid(), Some(o1));
        ob.place_order(Side::Buy, &o3);
        assert_eq!(ob.best_bid(), Some(o3));
        ob.place_order(Side::Buy, &o4);
        assert_eq!(ob.best_bid(), Some(o3));
    }
    #[test]
    fn basic_best_ask() {
        let mut ob = OrderBook::new();
        let o1 = Order::new(100.22, 20.0, 1);
        let o2 = Order::new(100.22, 20.0, 2);
        let o3 = Order::new(100.24, 20.0, 3);
        let o4 = Order::new(100.20, 20.0, 4);

        ob.place_order(Side::Sell, &o1);
        assert_eq!(ob.best_ask(), Some(o1));
        ob.place_order(Side::Sell, &o2);
        assert_eq!(ob.best_ask(), Some(o1));
        ob.place_order(Side::Sell, &o3);
        assert_eq!(ob.best_ask(), Some(o1));
        ob.place_order(Side::Sell, &o4);
        assert_eq!(ob.best_ask(), Some(o4));
    }

    #[test]
    fn order_replace() {
        let mut ob = OrderBook::new();
        let o1 = Order::new(100.22, 20.0, 1);
        let o2 = Order::new(100.22, 10.0, 1);

        ob.place_order(Side::Sell, &o1);
        assert_eq!(ob.best_ask(), Some(o1));
        ob.match_bid_order(&o2);
        assert_eq!(ob.best_ask(), Some(o2));

        let serialized = serde_json::to_string(&ob).unwrap();
        println!("serialized = {}", serialized);
    
        let deserialized: OrderBook = serde_json::from_str(&serialized).unwrap();
        println!("deserialized = {:?}", deserialized);
    }

}
