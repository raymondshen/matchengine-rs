use matchengine::match_engine_client::MatchEngineClient;
use matchengine::HelloRequest;

pub mod matchengine {
    tonic::include_proto!("matchengine");
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut client = MatchEngineClient::connect("http://[::1]:50051").await?;

    let request = tonic::Request::new(HelloRequest {
        name: "Tonic".into(),
    });

    let response = client.say_hello(request).await?;

    println!("RESPONSE={:?}", response);

    Ok(())
}
