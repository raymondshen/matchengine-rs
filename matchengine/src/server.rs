mod orderbook;
mod matchengine;

use matchengine::matchengine::match_engine_server::{MatchEngine, MatchEngineServer};
use tonic::{transport::Server, Request, Response, Status};
use matchengine::MatchEngineService;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:50051".parse()?;
    let service = MatchEngineService::new(1,"match_engine_cluster".to_string()).await;

    Server::builder()
        .add_service(MatchEngineServer::new(service))
        .serve(addr)
        .await?;

    Ok(())
}

/*
#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:50051".parse()?;
    let engine = MyEngine::default();

    Server::builder()
        .add_service(MatchEngineServer::new(engine))
        .serve(addr)
        .await?;

    Ok(())
}
*/