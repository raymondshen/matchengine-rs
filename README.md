# Match Engine Rust

Considering Raft is a good candididate for atomic broadcast protocal when implementing high-availability Match Engine, We found implementation in Rust language.

- [async-raft](https://github.com/async-raft/async-raft) is an acitvely developing library which implements high performance raft protocol in rust.
- [darksteel](https://github.com/darksteel-rs/darksteel) is a good demostration of async-raft as we didn't found much samples for async-raft library.

Based on these two projects above, we are introducing rust orderbook implementation. 

The whole projects is going to implement a snapshot/journal based Match Engine recover machenism and Raft distribute consensus. Which is cosiering very good performance in practise.

## Order Book
This is a order book which runs match process which supports:
- Limit order match.
- Serialize and Deserialize base on [serde](https://github.com/serde-rs/serde)

## Match Engine
The StateMachine could be synchronized to nodes by async-raft.


## TODO
- Add a gprc client.
- Enhance store for real persistence.
## Tips
- If you are tring to run current test cases, you need to set up ip alias using <code>ipalias.sh</code>
- Use  <code>cargo test -- --nocapture</code>  to show more debug output in test cases.

> **Following docs are from darksteel project, we are going to modify them later.**

-----
# Darksteel - Supervised task runtime
[![](https://docs.rs/darksteel/badge.svg)](https://docs.rs/darksteel/)

A high level task runtime that allows for increased observability
and reliability through supervision.

> **READ THIS**: Darksteel is in an alpha state, is missing some functionality,
> properly functioning test cases, and has incomplete documentation. Use at your
> own risk.

## Features
- [x] Task messaging.
- [x] Global module system.
- [x] Task supervision.
- [x] Distributed state implemented via Raft.

## License

Copyright 2021, Reese van Dijk and contributors.

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.